# Task

1. Connect to wss://ws.car24.uz/ws-random-2

2. You can get three type of message

    - progress

    - notification

    - message

3. Show the progress on the top left corner of the page

4. Show the notifications on the top right corner of the page

5. Show the messages on the **Content**
